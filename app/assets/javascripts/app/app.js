var app = angular.module('resourses',['ngRoute'])

app.config(['$routeProvider',function($routeProvider, $routeParams) {
	$routeProvider
		.when('/', {
			templateUrl: "/home",
			controller: "HomeCtrl"
		})
		.when('/articles', {
			templateUrl: "/articles",
			controller: "ArticlesCtrl"
		})
		.when('/articles/new', {
			templateUrl: "/articles/new",
			controller: "NewArticleCtrl"
		})
		.when('/articles/:id/edit', {
			templateUrl: function(params){ return '/articles/' + params.id +'/edit'; },
			controller: "EditArticleCtrl"
		})
		.when('/articles/:id', {
			templateUrl: function(params){ return '/articles/' + params.id;},
			controller: "ShowArticleCtrl"
		})
		.when('/contacts', {
			templateUrl: "/contacts",
			controller: "ContactsCtrl"
		})
		.when('/users', {
			templateUrl: "/users/edit",
			controller: "EditUserCtrl"
		})
}])


app.controller('HomeCtrl', ['$rootScope', function($rootScope){
	$rootScope.currentPage = 'home'
	
}])

app.controller('NewArticleCtrl', ['$rootScope', '$location', '$http', '$scope', function($rootScope, $location, $http, $scope){
	$rootScope.currentPage = 'articles';

	$scope.saveArticle = function (fd) {
		angular.element(document.querySelectorAll('.error')).remove();
		$http({method: "POST", url: "/articles.json", data: fd})
			.success(function (res) {
				console.log("OK ",res)
				$location.path('/articles')
			})
			.error(function (res) {
				_.each(res.errors, function (v, k) {
					angular.element(document.querySelector('.'+k)).append('<span class="error">'+v+'</span>')
				})
			})
	}
}])

app.controller('EditArticleCtrl', ['$rootScope', '$location', '$http', '$scope', '$routeParams', function($rootScope, $location, $http, $scope, $routeParams){
	$rootScope.currentPage = 'articles';

	$scope.saveArticle = function (fd) {
		angular.element(document.querySelectorAll('.error')).remove();
		$http({method: "PUT", url: "/articles/"+$routeParams.id+".json", data: fd})
			.success(function (res) {
				$location.path('/articles')
			})
			.error(function (res) {
				_.each(res.errors, function (v, k) {
					angular.element(document.querySelector('.'+k)).append('<span class="error">'+v+'</span>')
				})
			})
	}
}])

app.controller('ShowArticleCtrl', ['$rootScope', '$location', '$http', '$scope', '$routeParams', function($rootScope, $location, $http, $scope, $routeParams){
	$rootScope.currentPage = 'articles';
		$http({method: "GET", url: "/articles/"+$routeParams.id+".json"})
}])

app.controller('ArticlesCtrl', ['$scope', '$rootScope', '$http', function($scope, $rootScope, $http){
	$rootScope.currentPage = 'articles'

	$scope.doSearch = function (str) {
		$http({method: "GET", url: "/search", params: {q: str}}).success(function (res) {
			$scope.articles = res;
		})
	}

	$scope.remove = function (id) {
		var temp = "/articles/"+id+'.json';
		$http({method: "DELETE", url: temp}).success(function (res) {
			$scope.articles = res;
		})
	}

	$rootScope.$watch('search', function (val) {
		$scope.doSearch(val)
	})
}])

app.controller('EditUserCtrl', ['$rootScope', '$location', '$http', '$scope', '$routeParams','$route', function($rootScope, $location, $http, $scope, $routeParams,$route){
	$scope.saveUser = function (f) {
		console.log(f);
		$http({method: "PUT", url: "/users.json", data: f}).success(function (res) {
			 $route.reload();
			})
	}
}])

app.controller('ContactsCtrl', ['$rootScope', function($rootScope){
	$rootScope.currentPage = 'contacts'
}])